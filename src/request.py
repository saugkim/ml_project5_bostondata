
import requests
import json

def make_api_request(data):
    # url for api
    url = 'http://127.0.0.1:5000/predict'
    # make post request
    #r = requests.post(url,data=json.dumps(data))
    r = requests.post(url, json=data)
    return r.json()

input_data = [{ "CRIM":0.00632,"ZN":18.0,"INDUS":2.31,"CHAS":0.0,"NOX":0.538,"RM":6.575,"AGE":65.2,
                "DIS":4.09,"RAD":1.0,"TAX":296.0,"PTRATIO":15.3,"B":396.9,"LSTAT":4.98}]

make_api_request(input_data)
