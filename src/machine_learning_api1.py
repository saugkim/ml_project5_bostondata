
from flask import render_template, request, jsonify
from flask import Flask
import numpy as np
import traceback
import pickle
import pandas as pd
import os
 
 
# App definition
app = Flask(__name__)
 
# importing models
model_path = os.path.join(os.path.pardir, 'model')
model_file = os.path.join(model_path, 'model.pkl')
model_column_file = os.path.join(model_path, 'model_columns.pkl')
with open(model_file, 'rb') as f:
   classifier = pickle.load (f)
 
with open(model_column_file, 'rb') as f:
   model_columns = pickle.load (f)
 
@app.route('/')
def welcome():
   return 'Boston Housing Price prediction'
 
@app.route('/predict', methods=['POST','GET'])
def predict():
  
   if request.method == 'GET':
       return 'Prediction page'
 
   if request.method == 'POST':
       try:
           input = request.get_json()
           print(input)
           query_ = pd.get_dummies(pd.DataFrame(json_))
           query = query_.reindex(columns = model_columns, fill_value= 0)
           prediction = list(classifier.predict(query))
           print('prediction : {0:.2f}'.format(prediction[0]))
 
           return jsonify({
               'prediction':str(prediction)
           })
 
       except:
           return jsonify({
               'trace': traceback.format_exc()
               })
      
 
if __name__ == "__main__":
   app.run()
